'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../src');

describe('validateProps vs checkProps (same basic behavior)', () => {

  it('identifier keys', () => {
    const types = {};
    const check = checkProps(types);
    assert.strictEqual(check.isPropTypesChecker, true);
    const validate = validateProps(types);
    assert.strictEqual(validate.isPropTypesValidator, true);
    const areValid = areValidProps(types);
    assert.strictEqual(areValid.isPropTypesBooleanTester, true);
  });

  it('some valids', () => {
    const types = {
      derp: PropTypes.string.isRequiredOrNull,
    };
    const check = checkProps(types);
    const validate = validateProps(types);
    const areValid = areValidProps(types);
    const valids = [
      {derp: 'asdf'},
      {derp: ''},
      {derp: null},
    ];
    _.each(valids, valid => {
      const message = check(valid);
      assert.strictEqual(message, null);
      assert.doesNotThrow(() => validate(valid));
      const isValid = areValid(valid);
      assert.strictEqual(isValid, true);
    });
  });

  it('some invalids', () => {
    const types = {
      derp: PropTypes.string.isRequired,
      merp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
      ]).isRequired,
    };
    const check = checkProps(types);
    const validate = validateProps(types);
    const invalids = [
      {
        props: {derp: 'asdf'},
        mess: 'PropTypes validation error at [merp]: Prop isRequired (cannot be null or undefined)',
      },
      {
        props: {derp: ['asdf'], merp: 23},
        mess: 'PropTypes validation error at [derp]: Prop must be a string when included',
      },
    ];
    _.each(invalids, invalid => {
      const message = check(invalid.props);
      assert.strictEqual(message, invalid.mess);
      assert.throws(() => validate(invalid.props), new Error(invalid.mess));
    });
  });

  it('test & check on nested examples with variable output custom check', () => {
    const types = {
      derp: PropTypes.shape({
        merp: PropTypes.shape({
          merp: PropTypes.any.isRequired,
        }),
        smerp: PropTypes.shape({
          smerp: PropTypes.any.custom(() => {
            return 'a short error message';
          }),
        }),
        lerp: PropTypes.shape({
          lerp: PropTypes.any.custom(() => {
            return new Error('A short custom error');
          }),
        }),
        nerp: PropTypes.shape({
          nerp: PropTypes.any.custom(() => {
            throw new Error('A short custom error');
          }),
        }),
      }),
    };
    const invalids = [
      {
        val: {derp: {merp: {merp: null}}},
        mess: 'PropTypes validation error at [derp, merp, merp]: Prop isRequired (cannot be null or undefined)',
      },
      {
        val: {derp: {smerp: {smerp: 2}}},
        mess: 'PropTypes validation error at [derp, smerp, smerp]: a short error message',
      },
      {
        val: {derp: {lerp: {lerp: 2}}},
        mess: 'A short custom error',
      },
      {
        val: {derp: {nerp: {nerp: 2}}},
        mess: 'A short custom error',
      },
    ];
    const check = checkProps(types);
    const validate = validateProps(types);
    _.each(invalids, invalid => {
      const mess = check(invalid.val);
      assert.strictEqual(mess, invalid.mess);
      assert.throws(() => validate(invalid.val), new Error(invalid.mess));
    });
  });

  it('validateProps with custom function that returns/throws an error', () => {
    const err = new Error('the same instance');
    const types = {
      derp: PropTypes.shape({
        derp: PropTypes.shape({
          derp: PropTypes.any.custom(() => {
            throw err;
          }),
        }),
      }),
    };
    const check = checkProps(types);
    const mess = check({derp: {derp: {derp: 2}}});
    assert.strictEqual(mess, 'the same instance');
    const validate = validateProps(types);
    assert.throws(() => validate({derp: {derp: {derp: 2}}}));
    try {
      validate({derp: {derp: {derp: 2}}});
    } catch (thrownErr) {
      assert.strictEqual(err, thrownErr);
    }
  });

});
