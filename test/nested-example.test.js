'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
} = require('../src');

describe('nested example', () => {

  const exampleTypes = {
    derp: PropTypes.shape({
      nerp: PropTypes.arrayOf(
        PropTypes.shape({
          lerp: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
          ]),
        }),
      ).isRequired,
    }),
  };
  const check = checkProps(exampleTypes);

  it('check messages for different props (error vs message)', () => {
    const checkees = [
      {
        props: {},
        mess: null,
      },
      {
        props: {derp: {}},
        mess: 'PropTypes validation error at [derp, nerp]: Prop isRequired (cannot be null or undefined)',
      },
      {
        props: {derp: {nerp: null}},
        mess: 'PropTypes validation error at [derp, nerp]: Prop isRequired (cannot be null or undefined)',
      },
      {
        props: {derp: {nerp: []}},
        mess: null,
      },
      {
        props: {derp: {nerp: [23, 34, 'asdf']}},
        mess: null,
      },
      {
        props: {derp: {nerp: [{}]}},
        mess: null,
      },
      {
        props: {derp: {nerp: [{lerp: null}]}},
        mess: null,
      },
      {
        props: {derp: {nerp: [{lerp: {}}]}},
        mess: 'PropTypes validation error at [derp, nerp, 0, lerp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)',
      },
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

});
