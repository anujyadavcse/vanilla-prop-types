'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
} = require('../src');

describe('error-injector.test', () => {

  describe('options validation', () => {

    it('options - some valids', () => {
      const valids = [
        null,
        undefined,
        [], //  //
        23, // hmm.. don't really care that these does work
        {isWeak: true},
      ];
      const err = new Error('irrelevant error - just checking options values');
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps(PropTypes.string.error(err, valid)));
      });
    });

    it('options - some invalids', () => {
      const invalids = [
        {anyInvalidKey: true},
      ];
      const err = new Error('irrelevant error - just checking options values');
      _.each(invalids, invalid => {
        assert.throws(() => checkProps(PropTypes.string.error(err, invalid)), /Invalid error-injector options \[anyInvalidKey\]/);
      })
    });

  });

  describe('the big test', () => {

    const aarbError = new Error('aarb error');
    const exampleTypes = {
      aarb: PropTypes.string.error(aarbError),
      aarb2: PropTypes.string.error(() => aarbError),
      aarb3: PropTypes.string.error(aarbError.message),
      aarb4: PropTypes.string.error(() => aarbError.message),
      aarb5: PropTypes.string.error(() => {
        throw aarbError;
      }),
      aarb6: PropTypes.string.error(({eerp: 'this is not a valid error return object and is ignored as a value'})),
      aarb7: PropTypes.string.error(() => ({eerp: 'this is not a valid error return object and is ignored as a value'})),
      barb: PropTypes.shape({
        barb: PropTypes.string.error(new Error('barb error')),
      }),
      carb: PropTypes.shape({
        carb: PropTypes.string.error(new Error('bottom carb error')),
      }).error(new Error('TOP carb error')),
      darb: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string.error('bottom darb error'))),
      earb: PropTypes.arrayOf(
        PropTypes.arrayOf(PropTypes.string.error('bottom earb error')),
      ).error('TOP earb error'),
      farb: PropTypes.objectOf(PropTypes.objectOf(PropTypes.string.error('bottom farb error'))),
      garb: PropTypes.objectOf(
        PropTypes.objectOf(PropTypes.string.error('bottom garb error')),
      ).error('TOP garb error'),
      harb: PropTypes.shape({
        harb: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string.error('harb error'))),
      }),
      // allowed variants (nest once so "isRequired" type stuff does not spill out..)
      iarb: PropTypes.shape({
        iarb: PropTypes.string.isRequired.error('iarb error'),
      }),
      jarb: PropTypes.shape({
        jarb: PropTypes.string.isRequiredOrNull.error('jarb error'),
      }),
      karb: PropTypes.shape({
        karb: PropTypes.string.isRequired.custom(() => new Error('karb custom err')).error('karb error wins'),
      }),
      larb: PropTypes.shape({
        larb: PropTypes.string.isRequiredOrNull.custom(() => new Error('larb custom err')).error('larb error wins'),
      }),
      marb: PropTypes.shape({
        marb: PropTypes.string.custom(() => new Error('marb custom err')).error('marb error wins'),
      }),
      narb: PropTypes.shape({
        narb: PropTypes.string.custom(() => new Error('narb custom err')).isRequired.error('narb error wins'),
      }),
      oarb: PropTypes.shape({
        oarb: PropTypes.string.custom(() => new Error('oarb custom err')).isRequiredOrNull.error('oarb error wins'),
      }),
    };


    const check = checkProps(exampleTypes);
    const validate = validateProps(exampleTypes);


    it('aarb - simple example', () => {
      const val = {aarb: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'aarb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err, aarbError);
      }
    });

    it('aarb2 - simple example (but error generating function)', () => {
      const val = {aarb2: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'aarb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err, aarbError);
      }
    });

    it('aarb3 - simple example (string supplied -> error not preserved)', () => {
      const val = {aarb3: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'aarb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.notStrictEqual(err, aarbError);
        assert.deepStrictEqual(err, aarbError);
      }
    });

    it('aarb4 - simple example (string generating function supplied -> error not preserved)', () => {
      const val = {aarb4: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'aarb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.notStrictEqual(err, aarbError);
        assert.deepStrictEqual(err, aarbError);
      }
    });

    it('aarb5 - simple example (but error generating function that THROWS aarbError)', () => {
      const val = {aarb5: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'aarb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err, aarbError);
      }
    });

    it('aarb6 - invalid error generating object supplied', () => {
      const val = {aarb6: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'PropTypes validation error at [aarb6]: Invalid custom error generator (it must be either a string, an error, or a function that returns a string or error or throws an error). Original error message: Prop must be a string when included');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('aarb7 - invalid error generating function (returns invalid object) supplied', () => {
      const val = {aarb7: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'PropTypes validation error at [aarb7]: Invalid custom error generator function (it must return a string or error or throw an error). Original error message: Prop must be a string when included');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('barb - two tiers', () => {
      const val = {barb: {barb: 23}};
      const mess = check(val);
      assert.strictEqual(mess, 'barb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('carb - two tiers, both with errorGen, last one wins', () => {
      const val = {carb: {carb: 23}};
      const mess = check(val);
      assert.strictEqual(mess, 'TOP carb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('darb - two tiers arrayOf', () => {
      const val = {darb: [[23]]};
      const mess = check(val);
      assert.strictEqual(mess, 'bottom darb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('earb - two tiers arrayOf, last one wins', () => {
      const val = {earb: [[23]]};
      const mess = check(val);
      assert.strictEqual(mess, 'TOP earb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('farb - two tiers objectOf', () => {
      const val = {farb: {billy: {bob: 23}}};
      const mess = check(val);
      assert.strictEqual(mess, 'bottom farb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('garb - two tiers objectOf, last one wins', () => {
      const val = {garb: {billy: {bob: 23}}};
      const mess = check(val);
      assert.strictEqual(mess, 'TOP garb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('harb - a mix of hierarchal typs', () => {
      const val = {harb: {harb: [{bob: 23}]}};
      const mess = check(val);
      assert.strictEqual(mess, 'harb error');
      assert.throws(() => validate(val));
      try {
        validate(val)
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('iarb - string.isRequired.error', () => {
      const invalids = [null, undefined, 23, {}];
      _.each(invalids, invalid => {
        const val = {iarb: {iarb: invalid}};
        const mess = check(val);
        assert.strictEqual(mess, 'iarb error');
        assert.throws(() => validate(val));
        try {
          validate(val);
        } catch (err) {
          assert.strictEqual(err.message, mess);
        }
      });
    });

    it('jarb - string.isRequiredOrNull.error', () => {
      const invalids = [undefined, 23, {}];
      _.each(invalids, invalid => {
        const val = {jarb: {jarb: invalid}};
        const mess = check(val);
        assert.strictEqual(mess, 'jarb error');
        assert.throws(() => validate(val));
        try {
          validate(val);
        } catch (err) {
          assert.strictEqual(err.message, mess);
        }
      });
      const nullMess = check({jarb: {jarb: null}});
      assert.strictEqual(nullMess, null);
    });

    it('karb - string.isRequired.custom.error', () => {
      const val = {karb: {karb: 'anything normally valid'}};
      const mess = check(val);
      assert.strictEqual(mess, 'karb error wins');
      assert.throws(() => validate(val));
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('larb - string.isRequiredOrNull.custom.error', () => {
      const val = {larb: {larb: 'anything normally valid'}};
      const mess = check(val);
      assert.strictEqual(mess, 'larb error wins');
      assert.throws(() => validate(val));
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('marb - string.custom.error', () => {
      const val = {marb: {marb: 'anything normally valid'}};
      const mess = check(val);
      assert.strictEqual(mess, 'marb error wins');
      assert.throws(() => validate(val));
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('narb - string.custom.error', () => {
      const val = {narb: {narb: 'anything normally valid'}};
      const mess = check(val);
      assert.strictEqual(mess, 'narb error wins');
      assert.throws(() => validate(val));
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('oarb - string.custom.error', () => {
      const val = {oarb: {oarb: 'anything normally valid'}};
      const mess = check(val);
      assert.strictEqual(mess, 'oarb error wins');
      assert.throws(() => validate(val));
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

  });

  describe('checking that args get sent', () => {

    const val3Error = new Error('val3 custom error');
    const types = {
      val1: PropTypes.string.error((arg) => {
        assert.strictEqual(arg.prop, 23);
        assert.strictEqual(arg.propName, 'val1');
        assert.strictEqual(arg.checkRes.message, 'Prop must be a string when included');
        assert.deepStrictEqual(arg.checkRes.address, ['val1']);
        return 'this is the error that will get through for val1';
      }),
      val2: PropTypes.shape({
        subby: PropTypes.string.error((arg) => {
          assert.strictEqual(arg.prop, 27);
          assert.strictEqual(arg.propName, 'subby');
          assert.strictEqual(arg.checkRes.message, 'Prop must be a string when included');
          assert.deepStrictEqual(arg.checkRes.address, ['subby']);
          return 'this is the error that will get through for val2';
        }),
      }),
      val3: PropTypes.shape({
        suppy: PropTypes.string.custom(() => val3Error).error((arg) => {
          assert.strictEqual(arg.prop, 'actually a string');
          assert.strictEqual(arg.propName, 'suppy');
          assert.strictEqual(arg.checkRes, val3Error);
          return 'this is the error that will get through for val3';
        }),
      }),
    };
    const check = checkProps(types);
    const validate = validateProps(types);

    it('error generating function val1', () => {
      const val = {val1: 23};
      const mess = check(val);
      assert.strictEqual(mess, 'this is the error that will get through for val1')
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('error generating function val2', () => {
      const val = {val2: {subby: 27}};
      const mess = check(val);
      assert.strictEqual(mess, 'this is the error that will get through for val2')
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

    it('error generating function val3', () => {
      const val = {val3: {suppy: 'actually a string'}};
      const mess = check(val);
      assert.strictEqual(mess, 'this is the error that will get through for val3')
      try {
        validate(val);
      } catch (err) {
        assert.strictEqual(err.message, mess);
      }
    });

  });

  describe('isWeak option', () => {

    it('used in conjuction with inline custom function (but NO outer error injector)', () => {
      const check = checkProps({
        outer: PropTypes.shape({
          barb0: PropTypes.string.custom(({prop}) => !prop ? null : 'barb0 custom message'),
          barb1: PropTypes.string.custom(({prop}) => !prop ? null : 'barb1 custom message').error('barb1 error override'),
          barb2: PropTypes.string.custom(({prop}) => !prop ? null : 'barb2 custom message').error('barb2 error override', {isWeak: true}),

          carb0: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb0 custom error')),
          carb1: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb1 custom error')).error('carb1 error override'),
          carb2: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb2 custom error')).error('carb2 error override', {isWeak: true}),
        }),
      });
      const propsCases = [
        // the barbs
        {
          props: {outer: {barb0: 23}},
          mess: 'PropTypes validation error at [outer, barb0]: Prop must be a string when included',
        },
        {
          props: {outer: {barb0: 'derp'}},
          mess: 'PropTypes validation error at [outer, barb0]: barb0 custom message',
        },
        {
          props: {outer: {barb1: 23}},
          mess: 'barb1 error override',
        },
        {
          props: {outer: {barb1: 'derp'}},
          mess: 'barb1 error override',
        },
        {
          props: {outer: {barb2: 23}},
          mess: 'barb2 error override',
        },
        {
          props: {outer: {barb2: 'derp'}},
          mess: 'barb2 error override',
        },
        // the carbs
        {
          props: {outer: {carb0: 23}},
          mess: 'PropTypes validation error at [outer, carb0]: Prop must be a string when included',
        },
        {
          props: {outer: {carb0: 'derp'}},
          mess: 'carb0 custom error',
        },
        {
          props: {outer: {carb1: 23}},
          mess: 'carb1 error override',
        },
        {
          props: {outer: {carb1: 'derp'}},
          mess: 'carb1 error override',
        },
        {
          props: {outer: {carb2: 23}},
          mess: 'carb2 error override',
        },
        {
          props: {outer: {carb2: 'derp'}},
          mess: 'carb2 custom error', // <-- because isWeak
        },
      ];
      _.each(propsCases, propsCase => {
        const mess = check(propsCase.props);
        assert.strictEqual(mess, propsCase.mess);
      });

    });

    it('used in conjuction with inline custom function (and WITH outer error injector but NOT isWeak)', () => {
      const check = checkProps({
        outer: PropTypes.shape({
          barb0: PropTypes.string.custom(({prop}) => !prop ? null : 'barb0 custom message'),
          barb1: PropTypes.string.custom(({prop}) => !prop ? null : 'barb1 custom message').error('barb1 error override'),
          barb2: PropTypes.string.custom(({prop}) => !prop ? null : 'barb2 custom message').error('barb2 error override', {isWeak: true}),

          carb0: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb0 custom error')),
          carb1: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb1 custom error')).error('carb1 error override'),
          carb2: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb2 custom error')).error('carb2 error override', {isWeak: true}),
        }).error('outer error override'),
      });
      const propsCases = [
        // the barbs
        {
          props: {outer: {barb0: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {barb0: 'derp'}},
          mess: 'outer error override',
        },
        {
          props: {outer: {barb1: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {barb1: 'derp'}},
          mess: 'outer error override',
        },
        {
          props: {outer: {barb2: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {barb2: 'derp'}},
          mess: 'outer error override',
        },
        // the carbs
        {
          props: {outer: {carb0: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {carb0: 'derp'}},
          mess: 'outer error override',
        },
        {
          props: {outer: {carb1: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {carb1: 'derp'}},
          mess: 'outer error override',
        },
        {
          props: {outer: {carb2: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {carb2: 'derp'}},
          mess: 'outer error override',
        },
      ];
      _.each(propsCases, propsCase => {
        const mess = check(propsCase.props);
        assert.strictEqual(mess, propsCase.mess);
      });

    });

    it('used in conjuction with inline custom function (and WITH outer error injector AND isWeak)', () => {
      const check = checkProps({
        outer: PropTypes.shape({
          barb0: PropTypes.string.custom(({prop}) => !prop ? null : 'barb0 custom message'),
          barb1: PropTypes.string.custom(({prop}) => !prop ? null : 'barb1 custom message').error('barb1 error override'),
          barb2: PropTypes.string.custom(({prop}) => !prop ? null : 'barb2 custom message').error('barb2 error override', {isWeak: true}),

          carb0: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb0 custom error')),
          carb1: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb1 custom error')).error('carb1 error override'),
          carb2: PropTypes.string.custom(({prop}) => !prop ? null : new Error('carb2 custom error')).error('carb2 error override', {isWeak: true}),
        }).error('outer error override', {isWeak: true}),
      });
      const propsCases = [
        // the barbs
        {
          props: {outer: {barb0: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {barb0: 'derp'}},
          mess: 'outer error override',
        },
        {
          props: {outer: {barb1: 23}},
          mess: 'barb1 error override',
        },
        {
          props: {outer: {barb1: 'derp'}},
          mess: 'barb1 error override',
        },
        {
          props: {outer: {barb2: 23}},
          mess: 'barb2 error override',
        },
        {
          props: {outer: {barb2: 'derp'}},
          mess: 'barb2 error override',
        },
        // the carbs
        {
          props: {outer: {carb0: 23}},
          mess: 'outer error override',
        },
        {
          props: {outer: {carb0: 'derp'}},
          mess: 'carb0 custom error',
        },
        {
          props: {outer: {carb1: 23}},
          mess: 'carb1 error override',
        },
        {
          props: {outer: {carb1: 'derp'}},
          mess: 'carb1 error override',
        },
        {
          props: {outer: {carb2: 23}},
          mess: 'carb2 error override',
        },
        {
          props: {outer: {carb2: 'derp'}},
          mess: 'carb2 custom error', // <-- because isWeak
        },
      ];
      _.each(propsCases, propsCase => {
        const mess = check(propsCase.props);
        assert.strictEqual(mess, propsCase.mess);
      });

    });

    it('example case from Readme', () => {

      const morpTypes = {
        morp1: PropTypes.shape({
          snorp: PropTypes.string,
          knorp: PropTypes.string.error('knorp custom error'),
        }).error('injected custom error if an error not already being propagated up', {isWeak: true}),
        morp2: PropTypes.shape({
          snorp: PropTypes.string,
          knorp: PropTypes.string.error('knorp custom error'),
        }).error('always overrides every error'),
      };
      const checkMorps = checkProps(morpTypes);

      const propsCases = [
        {props: {}, mess: null},
        {props: {morp1: {}, morp2: {}}, mess: null},
        {props: {morp1: {snorp: 'a', knorp: 'b'}, morp2: {snorp: 'c', knorp: 'c'}}, mess: null},
        {props: {morp1: {snorp: 23}}, mess: 'injected custom error if an error not already being propagated up'},
        {props: {morp1: {knorp: 23}}, mess: 'knorp custom error'},
        {props: {morp2: {snorp: 23}}, mess: 'always overrides every error'},
        {props: {morp2: {knorp: 23}}, mess: 'always overrides every error'},
      ];
      _.each(propsCases, propsCase => {
        const mess = checkMorps(propsCase.props);
        assert.strictEqual(mess, propsCase.mess);
      });

    });

  });

});
