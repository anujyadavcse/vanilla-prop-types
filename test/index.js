// test/index.js
'use strict';

describe('prop-types', () => {

  it('testing prop-types has begun', () => 0);

  require('./config-input-validation.test');
  require('./simple-single-prop-checks.test');
  require('./comprehensive-example.test');
  require('./nested-example.test');
  require('./validate-vs-check.test');
  require('./error-injector.test');
  require('./creating-custom-prop-types.test');

});
