'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
} = require('../src');

describe('config input-validators', () => {

  describe('propTypes (default options)', () => {

    it('must be an object (default options)', () => {
      const valids = [
        {},
        [],
        () => 0,
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps(valid));
        assert(_.isFunction(checkProps(valid)));
      });
    });

    it('and some invalids (default options)', () => {
      const invalids = [
        0,
        'asdff',
        '',
        undefined,
        null,
      ];
      const mess = /propTypes must be an object/;
      _.each(invalids, invalid => {
        assert.throws(() => checkProps(invalid), mess);
      });
    });

    it('propTypes values must be propTypes functions', () => {
      const invalids = [
        {
          propTypes: {thisProp: 'not a prop-types function'},
          mess: /invalid propType function for {propName: thisProp}/,
        },
        {
          propTypes: {thisProp: () => 0},
          mess: /invalid propType function for {propName: thisProp}/,
        },
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps(invalid.propTypes), invalid.mess);
      })
    });

    it('Standard usage: props object', () => {
      const check1 = checkProps({derr: PropTypes.string});
      const mess11 = check1('asdf');
      assert.strictEqual(mess11, 'PropTypes validation error: The props container must be a plain object (since the propTypes specifier was a plain object): {props: asdf}');
      const mess12 = check1({});
      assert.strictEqual(mess12, null);
      const mess13 = check1({derr: 'asdf'});
      assert.strictEqual(mess13, null);
      const mess14 = check1({derr: 23});
      assert.strictEqual(mess14, 'PropTypes validation error at [derr]: Prop must be a string when included');
      const mess15 = check1(null);
      assert.strictEqual(mess15, 'PropTypes validation error: A null object is not allowed (see options to allow)');
      const mess16 = check1(undefined);
      assert.strictEqual(mess16, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
    });

  });

  describe('options', () => {

    it('some valids', () => {
      const valids = [
        {isNullable: true},
        {isNilable: true},
        {isExact: true},
        {blacklist: []},
        {blacklist: ['propA', 'propB', 'propC']},
        {custom: () => 0},
        {custom: false},
        {custom: null},
        {custom: false},
      ];
      _.each(valids, validOptions => {
        assert.doesNotThrow(() => checkProps({}, validOptions));
      });
    });

    it('some invalids', () => {
      const invalids = [
        {
          options: {invalidKey: true},
          mess: /Invalid options found:/,
        },
        {
          options: {blacklist: 23},
          mess: /Invalid blacklist, must be an array/,
        },
        {
          options: {custom: true},
          mess: /the global custom function must be a function if included/,
        },
        {
          options: {isExact: true, blacklist: ['dingo', 'blue']},
          mess: /Invalid options - "blacklist" is not allowed in conjunction with "isExact"/,
        }
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({}, invalid.options), invalid.mess);
      });
    });

    it('isNullable option', () => {
      const options = {isNullable: true};
      const check = checkProps({derp: PropTypes.string});
      const mess1 = check({derp: 'merp'});
      assert.strictEqual(mess1, null);
    });

    it('Standard usage: props object (BUT with isNilable)', () => {
      const check1 = checkProps({derr: PropTypes.string}, {isNilable: true});
      const mess11 = check1('asdf');
      assert.strictEqual(mess11, 'PropTypes validation error: The props container must be a plain object (since the propTypes specifier was a plain object): {props: asdf}');
      const mess12 = check1({});
      assert.strictEqual(mess12, null);
      const mess13 = check1({derr: 'asdf'});
      assert.strictEqual(mess13, null);
      const mess14 = check1({derr: 23});
      assert.strictEqual(mess14, 'PropTypes validation error at [derr]: Prop must be a string when included');
      const mess15 = check1(null);
      assert.strictEqual(mess15, null);
      const mess16 = check1(undefined);
      assert.strictEqual(mess16, null);
    });

    it('custom option (triple test..)', () => {
      const custom = ({props, propName, prop}) => {
        if (_.isNil(prop)) {
          return 'inside custom function: prop is nil';
        }
        if (propName !== 'global custom validation') {
          throw new Error('propName should always be "global custom validation"');
        }
        if (props !== undefined) {
          throw new Error('props should always be null for global custom functions');
        }
        // only props is really relevant
        if (prop.janks && prop.janks !== 'diggity') {
          throw new Error('janks should always be diggity if included');
        }
        if (prop.bobby) {
          return 'interesting';
        }
      };
      // first the check & validate
      const check = checkProps({}, {custom});
      const validate = validateProps({}, {custom});
      // some valids
      const valids = [
        {},
        {janks: null},
        {janks: 'diggity'},
        {bobby: false},
      ];
      _.each(valids, valid => {
        const mess = check(valid);
        assert.strictEqual(mess, null);
        assert.doesNotThrow(() => validate(valid));
      });

      // and some invalids
      const invalids = [
        {props: null, mess: 'PropTypes validation error: A null object is not allowed (see options to allow)'},
        {props: undefined, mess: 'PropTypes validation error: An undefined object is not allowed (see options to allow)'},
        {props: {janks: 'NOT diggity'}, mess: 'janks should always be diggity if included'},
        {props: {janks: true}, mess: 'janks should always be diggity if included'},
        {props: {bobby: true}, mess: 'PropTypes validation error at [global custom validation]: interesting'},
      ];
      _.each(invalids, invalid => {
        const mess = check(invalid.props);
        assert.strictEqual(mess, invalid.mess);
        try {
          validate(invalid.props);
          throw new Error('We should already have thrown an error');
        } catch (err) {
          assert.strictEqual(err.message, invalid.mess);
        }
      });

      // setting isNilable: true allows custom function to handle nil cases
      const check2 = checkProps({}, {custom, isNilable: true});
      const validate2 = validateProps({}, {custom, isNilable: true});
      const invalids2 = [
        {props: null, mess: 'PropTypes validation error at [global custom validation]: inside custom function: prop is nil'},
        {props: undefined, mess: 'PropTypes validation error at [global custom validation]: inside custom function: prop is nil'},
      ];
      _.each(invalids2, invalid => {
        const mess = check2(invalid.props);
        assert.strictEqual(mess, invalid.mess);
        try {
          validate2(invalid.props);
          throw new Error('We should already have thrown an error');
        } catch (err) {
          assert.strictEqual(err.message, invalid.mess);
        }
      })
    });

    it('custom & isNilable can be used to catch nil with custom error', () => {
      const custom = () => new Error('snerp the custom error');
      const check = checkProps({derp: PropTypes.any}, {custom, isNilable: true});
      const mess1 = check(null);
      assert.strictEqual(mess1, 'snerp the custom error');
      const mess2 = check(23);
      assert.strictEqual(mess2, 'snerp the custom error');
    });

  });

  describe('propTypes with options exceptions', () => {

    it('null object value', () => {
      // default options triggers error
      const defaultCheck = checkProps({});
      const defaultMessage = defaultCheck(null);
      assert.strictEqual(defaultMessage, 'PropTypes validation error: A null object is not allowed (see options to allow)');
      // nullable it sails through
      const nullableCheck = checkProps({}, {isNullable: true});
      const nullableMessage = nullableCheck(null);
      assert.strictEqual(nullableMessage, null);
      // nilable it sails through
      const nilableCheck = checkProps({}, {isNilable: true});
      const nilableMessage = nilableCheck(null);
      assert.strictEqual(nilableMessage, null);
    });

    it('undefined object value', () => {
      // default options triggers error
      const defaultCheck = checkProps({});
      const defaultMessage = defaultCheck(undefined);
      assert.strictEqual(defaultMessage, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
      // nullable it ALSO triggers error
      const nullableCheck = checkProps({}, {isNullable: true});
      const nullableMessage = nullableCheck(undefined);
      assert.strictEqual(nullableMessage, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
      // // nilable it sails through
      const nilableCheck = checkProps({}, {isNilable: true});
      const nilableMessage = nilableCheck(undefined);
      assert.strictEqual(nilableMessage, null);
    });

    it('trying to check propTypes of object with prop in blacklist fails', () => {
      const blacklist = ['propA', 'propB', 'propC', 'propD'];
      const check = checkProps({}, {blacklist});
      const mess1 = check({propA: 'boo'});
      assert.strictEqual(mess1, 'PropTypes validation error: Props with blacklisted propNames found: [propA]');
      const mess2 = check({propA: 'boo', propB: 'per', propC: 'asdf'});
      assert.strictEqual(mess2, 'PropTypes validation error: Props with blacklisted propNames found: [propA, propB, propC]');
      const mess3 = check({propA: 'boo', propB: 'per', propC: 'asdf', propD: 'boop'});
      assert.strictEqual(mess3, 'PropTypes validation error: Props with blacklisted propNames found: [propA, propB, propC, ...]');
    });

    it('isExact option not being followed', () => {
      const check = checkProps({
        red: PropTypes.any,
        blue: PropTypes.any,
      }, {
        isExact: true,
      });
      const invalids = [
        {props: {green: 0}, mess: 'PropTypes validation error: Props with isExact=true has extra (unallowed) props: [green]'},
        {props: {green: 0, black: 1, grey: 2}, mess: 'PropTypes validation error: Props with isExact=true has extra (unallowed) props: [green, black, grey]'},
        {props: {green: 0, black: 1, grey: 2, white: 3, purple: 4}, mess: 'PropTypes validation error: Props with isExact=true has extra (unallowed) props: [green, black, grey, ...]'},
      ];
      _.each(invalids, invalid => {
        const mess = check(invalid.props);
        assert.strictEqual(mess, invalid.mess);
      });
    });

  });

});
