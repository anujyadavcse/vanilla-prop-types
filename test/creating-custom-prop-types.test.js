'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
} = require('../src');

describe('creating custom PropTypes', () => {

  const CustomPropTypes = {
    favoriteShape: PropTypes.shape({
      arf: PropTypes.string,
      snarf: PropTypes.arrayOf(PropTypes.number.isRequired.custom(({prop}) => (prop > 0 ? null : 'bad snarf'))).isRequired,
    }),
    nonemptyArray: PropTypes.validator(validateProps(PropTypes.array.isRequired, {
      custom: ({prop}) => {
        if (prop.length === 0) {
          return 'invalid non-empty array';
        }
      },
    })),
    formattedString: PropTypes.validator(validateProps(PropTypes.string, {
      custom: ({prop}) => {
        if (!_.isString(prop) || !/asdf/.test(prop)) {
          return new Error('invalid formattedString');
        }
      },
    })),
  };

  it('favoriteShape (i.e. a standard solution) valids', () => {
    const check = checkProps(CustomPropTypes.favoriteShape.isRequired);
    const mess = check({
      arf: null,
      snarf: [23],
    });
    assert.strictEqual(mess, null);
  });

  it('favoritshape - some invalids', () => {
    const check = checkProps(CustomPropTypes.favoriteShape.isRequired);
    const invalids = [
      {val: {snarf: 0}, mess: 'PropTypes validation error at [, snarf]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {val: {snarf: [0]}, mess: 'PropTypes validation error at [, snarf, 0]: bad snarf'},
    ];
    _.each(invalids, invalid => {
      const mess = check(invalid.val);
      assert.strictEqual(mess, invalid.mess);
    });
  });

  it('nonemptyArray - valid', () => {
    const check = checkProps(CustomPropTypes.nonemptyArray);
    const mess = check(['1']);
    assert.strictEqual(mess, null);
  });

  it('nonemptyArray - invalids', () => {
    const check = checkProps(CustomPropTypes.nonemptyArray);
    const invalids = [
      {val: null, mess: 'PropTypes validation error: A null object is not allowed (see options to allow)'}, // hmmm.. allow null/nil for these?
      {val: {}, mess: 'PropTypes validation error at []: PropTypes validation error at []: Prop must be an array when included'},
      {val: [], mess: 'PropTypes validation error at []: PropTypes validation error at [global custom validation]: invalid non-empty array'}, // hardly ideal........
    ];
    _.each(invalids, invalid => {
      const mess = check(invalid.val);
      assert.strictEqual(mess, invalid.mess);
    });
  });

  it('formattedString - valid', () => {
    const check = checkProps(CustomPropTypes.formattedString);
    const mess = check('asdf........');
    assert.strictEqual(mess, null);
  });

  it('formattedString - invalids', () => {
    const check = checkProps(CustomPropTypes.formattedString);
    const invalids = [
      {val: null, mess: 'PropTypes validation error: A null object is not allowed (see options to allow)'},
      {val: 'dddd', mess: 'PropTypes validation error at []: invalid formattedString'},
    ];
    _.each(invalids, invalid => {
      const mess = check(invalid.val);
      assert.strictEqual(mess, invalid.mess);
    });
  });

});
