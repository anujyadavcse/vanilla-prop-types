// vsrc/index.js
'use strict';

const _ = require('lodash');
const isCallable = require('is-callable');

const MESSAGE_ITEMS_CROP = 3;

const PROP_TYPES_IDENTIFIER_KEYS = {
  isPropTypesFunction: 'isPropTypesFunction',
  isPropTypesChecker: 'isPropTypesChecker',
  isPropTypesValidator: 'isPropTypesValidator',
  isPropTypesBooleanTester: 'isPropTypesBooleanTester',
};

//
// options validation
//

const VALID_SHAPE_OPTIONS = {
  isExact: 'isExact', // no props other than those defined in shape are allowed
};
const validateShapeOptions = (options) => {
  const optionKeys = _.keys(options);
  const validOptionKeys = _.values(VALID_SHAPE_OPTIONS);
  const invalidKeys = _.without(optionKeys, ...validOptionKeys);
  if (_.size(invalidKeys)) {
    throw new Error(`Invalid shape options [${invalidKeys.join(', ')}]`);
  }
};

const VALID_ERROR_OPTIONS = {
  isWeak: 'isWeak', // if isWeak then error() only triggers on error-messages (NOT Errors)
};
const validateErrorOptions = (options) => {
  const optionKeys = _.keys(options);
  const validOptionKeys = _.values(VALID_ERROR_OPTIONS);
  const invalidKeys = _.without(optionKeys, ...validOptionKeys);
  if (_.size(invalidKeys)) {
    throw new Error(`Invalid error-injector options [${invalidKeys.join(', ')}]`);
  }
};


const VALID_MAIN_OPTIONS = {
  // The default is that PropTypes is used with an object.
  // However, exceptions can be made.
  isNullable: 'isNullable', // the object being checked is allowed to be null (default false)
  isNilable: 'isNilable', // the object being checked is allowed to be null or undefined (default false)
  isExact: 'isExact', // no props other than those with PropTypes are allowed (default false)
  blacklist: 'blacklist', // a list of propNames that are NOT allowed
  custom: 'custom', // a custom validation function can be injected from the getgo (that doesn't have to be attached to a specific prop)
};

const validateMainOptions = options => {
  const optionKeys = _.keys(options);
  const validOptionKeys = _.values(VALID_MAIN_OPTIONS);
  const invalidKeys = _.without(optionKeys, ...validOptionKeys);
  if (_.size(invalidKeys)) {
    throw new Error(`Invalid options found: [${invalidKeys.join(', ')}]`);
  }
  if (options[VALID_MAIN_OPTIONS.blacklist]) {
    if (options[VALID_MAIN_OPTIONS.isExact]) {
      throw new Error('Invalid options - "blacklist" is not allowed in conjunction with "isExact"');
    }
    const blacklist = options[VALID_MAIN_OPTIONS.blacklist];
    if (!_.isArray(blacklist)) {
      throw new Error(`Invalid blacklist, must be an array`);
    }
    _.each(blacklist, item => {
      if (!_.isString(item)) {
        throw new Error(`Invalid blacklist item, must be a string`);
      }
    });
  }
  if (options[VALID_MAIN_OPTIONS.custom]) {
    if (!_.isFunction(options[VALID_MAIN_OPTIONS.custom])) {
      throw new Error(`the global custom function must be a function if included`);
    }
  }
};

//
// mutators - a little dirty (?) but convenient
//

const addPropTypesIdentifier = (propChecker, identifierKey = PROP_TYPES_IDENTIFIER_KEYS.isPropTypesFunction) => {
  // mutates!!
  propChecker[identifierKey] = true;
};

//
// wrappers - provides the chaining
//

const wrapRequired = (propChecker) => {
  const wrappedChecker = ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return {address: [propName], message: 'Prop isRequired (cannot be null or undefined)'};
      // return `${propName} is required - cannot be null or undefined`;
    }
    return propChecker({prop, propName, props});
  };
  addPropTypesIdentifier(wrappedChecker);
  return wrappedChecker;
};

const wrapRequiredOrNull = (propChecker) => {
  const wrappedChecker = ({prop, propName, props}) => {
    if (_.isUndefined(prop)) {
      return {address: [propName], message: 'Prop is isRequiredOrNull (cannot be undefined)'};
      // return `${propName} isRequiredOrNull - cannot be undefined`;
    }
    return propChecker({prop, propName, props});
  };
  addPropTypesIdentifier(wrappedChecker);
  return wrappedChecker;
};

// injects custom error override
const wrapErrorGen = (propChecker) => {
  const wrappedCheckerGen = (errorGen, options = {}) => {
    validateErrorOptions(options);
    const wrappedChecker = ({prop, propName, props}) => {
      // first do the primary check
      const checkRes = propChecker({prop, propName, props});
      if (!checkRes) {
        return null;
      }
      if (_.isError(checkRes) && options[VALID_ERROR_OPTIONS.isWeak]) {
        return checkRes;
      }
      // extract/generate the custom error object
      if (_.isString(errorGen)) {
        const error = new Error(errorGen);
        return error;
      }
      if (_.isError(errorGen)) {
        const error = errorGen;
        return error;
      }
      if (_.isFunction(errorGen)) {
        // we do NOT want to "try" the supplied function bofore it is needed
        let errorGenRes;
        try {
          errorGenRes = errorGen({prop, propName, props, checkRes});
        } catch (err) {
          const error = err;
          return error;
        }
        if (_.isString(errorGenRes)) {
          const error = new Error(errorGenRes);
          return error;
        }
        if (_.isError(errorGenRes)) {
          const error = errorGenRes;
          return error;
        }
        return {
          address: checkRes.address,
          message: `Invalid custom error generator function (it must return a string or error or throw an error). Original error message: ${checkRes.message}`,
        };
      }
      return {
        address: checkRes.address,
        message: `Invalid custom error generator (it must be either a string, an error, or a function that returns a string or error or throws an error). Original error message: ${checkRes.message}`,
      };
    };
    addPropTypesIdentifier(wrappedChecker);
    return wrappedChecker;
  };
  return wrappedCheckerGen;
};

const wrapCustom = (propChecker, options) => {
  const wrappedCheckerGen = (customFn) => {
    const customChecker = isCustom(customFn);
    const wrappedChecker = ({prop, propName, props}) => {
      // first do the primary check
      const checkRes = propChecker({prop, propName, props});
      if (checkRes !== null) {
        return checkRes;
      }
      // then do the custom check
      const customCheckRes = customChecker({prop, propName, props});
      return customCheckRes;
    };
    addPropTypesIdentifier(wrappedChecker);
    wrappedChecker.error = wrapErrorGen(wrappedChecker);
    if (options.extend) {
      wrappedChecker.isRequired = wrapRequired(wrappedChecker);
      wrappedChecker.isRequiredOrNull = wrapRequiredOrNull(wrappedChecker);
      wrappedChecker.isRequired.error = wrapErrorGen(wrappedChecker.isRequired);
      wrappedChecker.isRequiredOrNull.error = wrapErrorGen(wrappedChecker.isRequiredOrNull);
    }
    return wrappedChecker;
  };
  return wrappedCheckerGen;
};

const wrapFinal = (propChecker) => {
  const wrappedChecker = ({prop, propName, props}) => propChecker({prop, propName, props});

  wrappedChecker.isRequired = wrapRequired(wrappedChecker);
  wrappedChecker.isRequiredOrNull = wrapRequiredOrNull(wrappedChecker);

  wrappedChecker.custom = wrapCustom(wrappedChecker, {extend: true});
  wrappedChecker.isRequired.custom = wrapCustom(wrappedChecker.isRequired, {extend: false});
  wrappedChecker.isRequiredOrNull.custom = wrapCustom(wrappedChecker.isRequiredOrNull, {extend: false});

  wrappedChecker.error = wrapErrorGen(wrappedChecker);
  wrappedChecker.isRequired.error = wrapErrorGen(wrappedChecker.isRequired);
  wrappedChecker.isRequiredOrNull.error = wrapErrorGen(wrappedChecker.isRequiredOrNull);


  addPropTypesIdentifier(wrappedChecker);
  return wrappedChecker;
};

//
// simple checkers
//

const isAny = ({prop, propName, props}) => {
  return null;
};

const isString = ({prop, propName, props}) => {
  if (_.isString(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be a string when included'};
  // return `${propName} is not a string`;
};

const isBoolean = ({prop, propName, props}) => {
  if (_.isBoolean(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be a bool when included'};
  // return `${propName} is not a bool`;
};

const isNumber = ({prop, propName, props}) => {
  if (_.isNumber(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be a number when included'};
  // return `${propName} is not a number`;
};

const isSymbol = ({prop, propName, props}) => {
  if (_.isSymbol(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be a symbol when included'};
  // return `${propName} is not a symbol`;
};

const isFunction = ({prop, propName, props}) => {
  if (_.isFunction(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be a function when included'};
  // return `${propName} is not a function`;
};

const isObject = ({prop, propName, props}) => {
  if (_.isObject(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be an object when included'};
  // return `${propName} is not an object`;
};

const isPlainObject = ({prop, propName, props}) => {
  if (_.isPlainObject(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be a plain object when included'};
  // return `${propName} is not a plain object`;
};

const isArray = ({prop, propName, props}) => {
  if (_.isArray(prop) || _.isNil(prop)) {
    return null;
  }
  return {address: [propName], message: 'Prop must be an array when included'};
  // return `${propName} is not an array`;
};

const validateEnum = propEnum => {
  if (!_.isArray(propEnum)) {
    throw new Error('propEnum must be an array');
  }
  if (propEnum.length !== _.uniq(propEnum).length) {
    throw new Error('propEnum must contain unique values');
  }
  const compactedEnum = _.filter(propEnum, val => !_.isNil(val));
  if (propEnum.length !== compactedEnum.length) {
    throw new Error('propEnum cannot contain undefined or null');
  }
};
const isOneOf = propEnum => {
  validateEnum(propEnum);
  const checker = ({prop, propName, props}) => {
    if (_.includes(propEnum, prop) || _.isNil(prop)) {
      return null;
    }
    const isCropped = propEnum.length > MESSAGE_ITEMS_CROP;
    const messageItems = !isCropped ? propEnum : propEnum.slice(0, MESSAGE_ITEMS_CROP);
    return {
      address: [propName],
      message: `Prop must be contained in enum: [${messageItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
    };
    // return `${propName} value is not contained in propEnum`;
  };
  const wrappedChecker = wrapFinal(checker);
  return wrappedChecker;
};

const validateClass = propClass => {
  if (!isCallable(propClass) || !propClass.prototype) {
    throw new Error('The Class must be a callable');
  }
};
const isInstanceOf = propClass => {
  validateClass(propClass);
  const checker = ({prop, propName, props}) => {
    if (prop instanceof propClass || _.isNil(prop)) {
      return null;
    }
    return {
      address: [propName],
      message: `Prop must be an instance of class: ${propClass}`,
    };
    // return `${propName} value is not an instance of the class`;
  };
  const wrappedChecker = wrapFinal(checker);
  return wrappedChecker;
};

//
// aggregate checkers
//

const isArrayOf = propType => {
  validatePropTypes({propType});
  const checker =  ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return null;
    }
    if (!_.isArray(prop)) {
      return {
        address: [propName],
        message: 'Prop must be an array (PropTypes.arrayOf requirement)',
      };
      // return `${propName} is not an array - should have items with given type`;
    }
    let invalidItemRes = null;
    _.each(prop, (item, index) => {
      invalidItemRes = propType({prop: item, propName: index, props: prop});
      if (invalidItemRes) {
        return false; // early exit
      }
    });
    if (!invalidItemRes) {
      return null;
    }
    // pass errors upwards directly
    if (_.isError(invalidItemRes)) {
      return invalidItemRes;
    }
    // aggregate message & address info
    return {
      address: [propName, ...invalidItemRes.address],
      message: invalidItemRes.message,
    };
  };
  const wrappedChecker = wrapFinal(checker);
  return wrappedChecker;
};

const isObjectOf = propType => {
  validatePropTypes({propType});
  const checker =  ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return null;
    }
    if (!_.isObject(prop)) {
      return {
        address: [propName],
        message: 'Prop must be an object (PropTypes.objectOf requirement)',
      };
      // return `${propName} is not an object - should have props with given type`;
    }
    let invalidItemRes = null;
    _.each(prop, (item, itemName) => {
      invalidItemRes = propType({prop: item, propName: itemName, props: prop});
      if (invalidItemRes) {
        return false; // early exit
      }
    });
    if (!invalidItemRes) {
      return null;
    }
    // pass errors upwards directly
    if (_.isError(invalidItemRes)) {
      return invalidItemRes;
    }
    // aggregate message & address info
    return {
      address: [propName, ...invalidItemRes.address],
      message: invalidItemRes.message,
    };
  };
  const wrappedChecker = wrapFinal(checker);
  return wrappedChecker;
};

const isOneOfType = propTypesArray => {
  if (!_.isArray(propTypesArray)) {
    throw new Error(`the "oneOfType" input must be an array of PropTypes`);
  }
  if (!_.size(propTypesArray)) {
    throw new Error(`the "oneOfType" input array must have at least one PropTypes item`);
  }
  validatePropTypes(propTypesArray);
  const checker = ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return null;
    }
    let foundValidType = false;
    _.each(propTypesArray, propType => {
      const checkRes = propType({prop, propName, props});
      if (!checkRes) {
        foundValidType = true;
        return false; // early exit
      }
    });
    if (foundValidType) {
      return null;
    }
    return {
      address: [propName],
      message: `Prop must be one of ${propTypesArray.length} given PropTypes (PropTypes.oneOfType requirement)`,
    };
  };
  const wrappedChecker = wrapFinal(checker);
  return wrappedChecker;
};


const extractExtraPropNames = (props, propTypes) => {
  const propNames = _.keys(props);
  const allowedPropNames = _.keys(propTypes);
  const extraPropNames = _.without(propNames, ...allowedPropNames);
  return extraPropNames;
};

const isShape = (propsShape, options = {}) => {
  if (!_.isObject(propsShape)) {
    throw new Error(`the "shape" input must be an object with PropTypes as props`);
  }
  validatePropTypes(propsShape);
  validateShapeOptions(options);
  const checker = ({prop, propName, props}) => {
    if (_.isNil(prop)) {
      return null;
    }
    const extraPropNames = extractExtraPropNames(prop, propsShape);
    if (options[VALID_SHAPE_OPTIONS.isExact] && extraPropNames.length) {
      const isCropped = extraPropNames.length > MESSAGE_ITEMS_CROP;
      const messageItems = !isCropped ? extraPropNames : extraPropNames.slice(0, MESSAGE_ITEMS_CROP);
      return {
        address: [propName],
        message: `PropType.shape with isExact=true has extra (unallowed) props: [${messageItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
      };
    }
    let invalidSubPropRes = null;
    _.each(propsShape, (subPropType, subPropName) => {
      const subProp = _.get(prop, subPropName);
      invalidSubPropRes = subPropType({prop: subProp, propName: subPropName, props: prop});
      if (invalidSubPropRes) {
        return false; // early return
      }
    });
    if (!invalidSubPropRes) {
      return null;
    }
    // pass errors upwards directly
    if (_.isError(invalidSubPropRes)) {
      return invalidSubPropRes;
    }
    // aggregate messages as an address
    return {
      address: [propName, ...invalidSubPropRes.address],
      message: invalidSubPropRes.message,
    };
  };
  const wrappedChecker = wrapFinal(checker);
  return wrappedChecker;
};


const isPropTypesValidator = (validate) => {
  if (!_.isFunction(validate)) {
    throw new Error(`the validate function must be a function`);
  }
  if (!validate[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesValidator]) {
    throw new Error(`the validate function must be (at least nominally) a PropTypes validator (i.e. created by the validateProps function)`);
  }
  const checker = (...args) => {
    const {prop, propName, props} = args[0] || {};
    let error;
    try {
      validate(prop);
    } catch (err) {
      error = err;
    }
    if (!error) {
      return null;
    }
    // revert back to an accumulating message
    return {
      address: [propName],
      message: error.message,
    };
  };
  const wrappedChecker = wrapFinal(checker);
  return wrappedChecker;
};


const isCustom = customFn => {
  if (!_.isFunction(customFn)) {
    throw new Error(`the custom function must be a function`);
  }
  const checker = ({prop, propName, props}) => {
    let res = undefined;
    try {
      res = customFn({prop, propName, props});
    } catch (err) {
      res = err;
    }
    if (_.isNil(res)) {
      return null;
    }
    // handle boolean cases so that new Boolean(true/false) and true/false both work..
    if (_.isBoolean(res) && res) {
      return null;
    }
    if (_.isError(res)) {
      return res; // pass on errors upwards differently from strings
    }
    if (_.isBoolean(res) && !res) {
      return {
        address: [propName],
        message: 'Prop fails custom validation function',
      };
    }
    if (_.isString(res)) {
      return {
        address: [propName],
        message: res,
      };
    }
    return {
      address: [propName],
      message: `Prop custom validation function returns ${res} which is forbidden - for validation success return one of {true, null, undefined} and for validation failure return one of {false, <string>, <error>}`,
    };
  };
  addPropTypesIdentifier(checker);
  return checker;
};


// generate the PropTypes - fully massaged
const PropTypes = {
  any: wrapFinal(isAny),
  string: wrapFinal(isString),
  bool: wrapFinal(isBoolean),
  number: wrapFinal(isNumber),
  symbol: wrapFinal(isSymbol),
  func: wrapFinal(isFunction),
  object: wrapFinal(isObject),
  plainObject: wrapFinal(isPlainObject),
  array: wrapFinal(isArray),
  oneOf: isOneOf,
  instanceOf: isInstanceOf,
  arrayOf: isArrayOf,
  objectOf: isObjectOf,
  oneOfType: isOneOfType,
  shape: isShape,
  validator: isPropTypesValidator,
  custom: isCustom,
};

//
// checker config validation
//

const validatePropTypes = (propTypes) => {
  if (!_.isObject(propTypes)) {
    throw new Error('propTypes must be an object');
  }
  if (_.isFunction(propTypes) && propTypes.isPropTypesFunction) {
    return; // good to go
  }
  // allow single value objects
  _.each(propTypes, (propType, propName) => {
    if (!_.isFunction(propType) || !propType.isPropTypesFunction) {
      throw new Error(`invalid propType function for {propName: ${propName}}`);
    }
  });
};

// a tiny wrapper for the special custom validator included in options object
const createGlobalCustomCheck = customFn => {
  if (!customFn) {
    return null; // is not assumed to be included
  }
  const customCheck = isCustom(customFn);
  const globalCustomCheck = (globalProps) => {
    return customCheck({
      prop: globalProps,
      props: undefined,
      propName: 'global custom validation',
    });
  };
  return globalCustomCheck;
};

// the main guy
const internalCheckProps = (propTypes, options = {}) => {
  validatePropTypes(propTypes);
  validateMainOptions(options);
  // create a global custom validation checker (or null)
  const customFn = options[VALID_MAIN_OPTIONS.custom];
  const globalCustomCheck = createGlobalCustomCheck(customFn);
  // creates a function that checks input props object. does NOT throw errors
  const check = (props) => {
    // special values (null & undefined)
    if (_.isNull(props)) {
      const canBeNull = options[VALID_MAIN_OPTIONS.isNullable] || options[VALID_MAIN_OPTIONS.isNilable];
      if (canBeNull) {
        return globalCustomCheck ? globalCustomCheck(props) : null;
      }
      return {
        address: [],
        message: 'A null object is not allowed (see options to allow)',
      };
    }
    if (_.isUndefined(props)) {
      const canBeUndefined = options[VALID_MAIN_OPTIONS.isNilable];
      if (canBeUndefined) {
        return globalCustomCheck ? globalCustomCheck(props) : null;
      }
      return {
        address: [],
        message: 'An undefined object is not allowed (see options to allow)',
      };
    }
    // case of a single PropTypeFunction
    if (_.isFunction(propTypes) && propTypes.isPropTypesFunction) {
      const message = propTypes({
        prop: props,
        props: null,
        propName: null,
      });
      if (message) {
        return message;
      }
      // last of all run the global options.custom check
      return globalCustomCheck ? globalCustomCheck(props) : null;
    }
    // some edge cases - to get here _.isNil(props) (with isNilable) or _.isPlainObject(props)
    if (!_.isPlainObject(props)) {
      if (!_.isNil(props)) {
        return globalCustomCheck ? globalCustomCheck(props) : {
          address: [],
          message: `The props container must be a plain object (since the propTypes specifier was a plain object): {props: ${props}}`,
        };
      }
      // _.isNil - just do the global options.custom check
      return globalCustomCheck ? globalCustomCheck(props) : null;
    }
    // isExact check
    const extraPropNames = extractExtraPropNames(props, propTypes);
    if (options[VALID_MAIN_OPTIONS.isExact] && extraPropNames.length) {
      const isCropped = extraPropNames.length > MESSAGE_ITEMS_CROP;
      const messageItems = !isCropped ? extraPropNames : extraPropNames.slice(0, MESSAGE_ITEMS_CROP);
      return {
        address: [],
        message: `Props with isExact=true has extra (unallowed) props: [${messageItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
      };
    }
    // blacklist check
    if (options[VALID_MAIN_OPTIONS.blacklist]) {
      const blacklist = options[VALID_MAIN_OPTIONS.blacklist];
      const propNames = _.keys(props);
      const blacklistedProps = _.intersection(propNames, blacklist);
      if (_.size(blacklistedProps)) {
        const isCropped = blacklistedProps.length > MESSAGE_ITEMS_CROP;
        const messageItems = !isCropped ? blacklistedProps : blacklistedProps.slice(0, MESSAGE_ITEMS_CROP);
        return {
          address: [],
          message: `Props with blacklisted propNames found: [${messageItems.join(', ')}${!isCropped ? '' : ', ...'}]`,
        };
      }
    }
    // and run the props checks
    let generalCheckRes = null;
    _.each(propTypes, (checker, propName) => {
      const prop = props[propName];
      const checkRes = checker({prop, propName, props});
      if (checkRes) {
        generalCheckRes = checkRes;
        return false; // early exit
      }
    });
    if (generalCheckRes) {
      return generalCheckRes;
    }
    // last of all run the global options.custom check
    return globalCustomCheck ? globalCustomCheck(props) : null;
  };
  // and done
  return check;
};

const assembleCheckerMessage = checkRes => {
  if (!checkRes) {
    return null;
  }
  if (_.isError(checkRes)) {
    return checkRes.message;
  }
  if (!_.isObject(checkRes) || !_.isArray(checkRes.address) || !_.isString(checkRes.message)) {
    throw new Error('DevErr: assembleCheckerMessage arg looks invalid');
  }
  let message;
  if (!checkRes.address.length) {
    message = `PropTypes validation error: ${checkRes.message}`;
  } else {
    message = `PropTypes validation error at [${checkRes.address.join(', ')}]: ${checkRes.message}`;
  }
  return message;
};

// ////
// exports
// ////

const checkProps = (propTypes, options) => {
  const internalCheck = internalCheckProps(propTypes, options);
  const check = (props) => {
    const checkRes = internalCheck(props);
    const message = assembleCheckerMessage(checkRes);
    return message;
  };
  addPropTypesIdentifier(check, PROP_TYPES_IDENTIFIER_KEYS.isPropTypesChecker);
  return check;
};

const validateProps = (propTypes, options) => {
  const internalCheck = internalCheckProps(propTypes, options);
  const validate = (props) => {
    const checkRes = internalCheck(props);
    if (!checkRes) {
      return;
    }
    if (_.isError(checkRes)) {
      throw checkRes;
    }
    const message = assembleCheckerMessage(checkRes);
    throw new Error(message);
  };
  // and done
  addPropTypesIdentifier(validate, PROP_TYPES_IDENTIFIER_KEYS.isPropTypesValidator);
  return validate;
};

const areValidProps = (propTypes, options) => {
  const internalCheck = internalCheckProps(propTypes, options);
  const areValid = (props) => {
    const checkRes = internalCheck(props);
    return !checkRes;
  };
  addPropTypesIdentifier(areValid, PROP_TYPES_IDENTIFIER_KEYS.isPropTypesBooleanTester);
  return areValid;
};


//
// Exports
//

module.exports = {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
};
